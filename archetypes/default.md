---
title: "{{ .Name | humanize }}"
date: {{ .Date }}
url: "blog/{{ .Name }}"
keyword: ""
tags: []
hero: ""
color:
  base: ""
  alt: ""
images:
- /blog/{{ .Name }}/image.xxx
draft: true
---
