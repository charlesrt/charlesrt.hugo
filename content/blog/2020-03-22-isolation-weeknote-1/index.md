---
title: "Isolation: Weeknote One"
date: 2020-03-22T12:13:49Z
description: "My family and I made the decision to isolate as a household due to the news of the (COVID-19) coronavirus. We took our son out of school, our daughter out of nursery and decided we were going to work from home for the foreseeable."
url: "blog/isolation-weeknote-1"
tags: [weeknotes, remote, isolation]
hero: "/blog/isolation-weeknote-1/rainbow.jpg"
images:
- /blog/isolation-weeknote-1/rainbow.jpg
draft: true

---

My family and I made the decision to isolate as a household due to the news of the (COVID-19) coronavirus. We took our son out of school, our daughter out of nursery and decided we were going to work from home for the foreseeable. This was 24 hours before the government started recommending people work from home if they can. At first we wondered if we were being paranoid, then when the government recommendation came, we worried if we'd acted soon enough.

We did this because our son has chronic kidney and following a successful transplant in 2019 he takes daily immunosuppressants to stop his body rejecting his new kidney. The same medicine that is keeping his kidney going also makes him very defenceless against viruses.

{{< figure src="rainbow.jpg" caption="" >}}

Monday was chaotic, not just because we now had the whole family at home but because work was continuity planning in the event of isolation. I was open with my team about the decision I'd made while trying to reassure them that everything at work was continuing as usual for now. Around 21:00 that night myself and other senior managers received confirmation that we were running a remote working resilience test the very next day and people were not to come into the office. What followed was lots of scrambling activity to try and notify all 16 of my team. It was around 22:00 now so I didn't want to call anyone. I sent text messages to people whose numbers I could find. Direct messaged everyone on Slack and sent emails too in the hope that at least one channel would reach people before they set off for work in the morning. Once finished I went to bed around 23:00 but I did not sleep for hours; I was alert with a multitudes of thoughts running around my head.

Luckily, everyone got the message in some format and stayed home for the resilience test which lasted Tuesday and Wednesday before finally being confirmed as the new normal as the advice from the government escalated each day.

DO I WANT TO FINISH THIS??
