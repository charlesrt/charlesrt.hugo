---
title: "Design Sprint in a day"
date: 2017-09-12T07:00:00+01:00
url: "blog/design-sprint-in-a-day"
keyword: "day"
tags: [design sprint, time, training]
---

Jake Knapp used to work at [Google Ventures](https://www.gv.com). During that time Jake developed a process to help teams test their ideas and get market feedback early, rather than spend years designing and building a thing before getting any data on whether it works. The process that came to be, was a Design Sprint.

{{< figure src="charles-chris-jake.jpg" caption="(Left to right) Charles Reynolds-Talbot, Chris Taylor and Jake Knapp" >}}

Since that time Jake has run over 150 Design Sprints with teams in a variety of companies, from Slack to Blue Bottle Coffee, and published a book about the process called [Sprint](https://www.thesprintbook.com).

Recently, Jake left Google Ventures to pursue a passion for writing, whilst consulting and running workshop training sessions on the Design Sprint process. One of those workshops was a single day session at the beautiful Tate Modern in London, [presented by Clearleft](https://clearleft.com/events).

By now, you've probably guessed that this post isn't a guide on how to run a Design Sprint in a day---I wouldn't recommend that. You might be able to condense certain elements to make for an efficient and productive day, but it's not a Design Sprint. You need to invest a full five days of time into a Design Sprint to reap the rewards the process provides.

This post is instead a review of Jake's workshop and a recommendation that---if you've read this far---you should attend one, given the opportunity.

{{< figure src="introductions.jpg" caption="Welcoming the teams" >}}

## Life by Keynote

The day started with Jake giving a bit of a history lesson about him and his experiences that led to the creation of the Design Sprint process, before getting into the detail on what you should do as a facilitator on each of the five days.

Throughout this, Jake had crafted a series of stories that helped to give context and perspective to each part of the process, supported by a well-designed slide deck and a light-hearted delivery.

Far from Death by Powerpoint, I was surprised at how engaged I was throughout. I don't think I yawned once. This is important right, because when you're bored you disengage. When you disengage you switch off, and then you're not learning anymore. Jake keeps your attention in the palm of his hand throughout and as a result delivers an incredibly refreshing learning experience.

Even the icebreaker was genius. I won't spoil it, you'll just have to attend one the workshops.

{{< figure src="life-by-keynote.jpg" caption="Life by Keynote" >}}

## Validating interpretations

The single-day workshop I attended focussed on the hardest days---Monday and Wednesday---where you map the flow from discovery to your customers/users achieving their goal (Monday) and make a decision on the moment in time (Wednesday) to focus your prototype and test on.

Tuesdays sketching exercises were another main feature of the workshop, probably because they're practical and therefore easy to get a room involved in. I got less value from this section as it's one of the days of the Design Sprint I find [surprisingly] straight forward. Especially when people I think will be reluctant to sketch, get involved no problem.

Jake did unpack some more detail behind the Lightning Demos, which was welcome to see a focus on using agnostic design patterns for inspiration.

{{< figure src="art-museum.jpg" caption="Our art was literally on display at the Tate Modern" >}}

Prototype and test (Thursday and Friday) were covered at the end by way of a whole room discussion, which was useful to hear other peoples questions and pain points from the process.

Jake also talked about things that didn't made the final cut of the Sprint book, such as metrics. While metrics might be too specific to quantify for some teams, it is important to have measurable outcomes so you know you've succeeded in doing the thing you set out to do.

Throughout the day; a combination of Jake diving into the detail and practical team exercises you get familiar with the nuances of the process which you don't necessarily get from reading the Sprint book.

I've read the physical book and have a digital copy on my iPhone for easy reference. I have run several Design Sprints myself and even [wrote about one I did in government that was published on sprintstories.com](https://sprintstories.com/running-a-design-sprint-in-government-8633bb390779). And---if I'm honest---while the day for me was *really* an excuse to meet the legend himself and get a signed copy of the Sprint book, I still found it invaluable.

It's important to validate your own interpretations from reading the book and running your own Design Sprints.

## Designing time

One of the key messages I took away from the day was the true value of a Design Sprint.

Ideas and data about a product or service might be the obvious outcome you get from running one, but a Design Sprint is *really* about designing time.

Design Sprints help people save time.

{{< figure src="sprint-signed.jpg" caption="🙌" >}}
