---
title: "Dat’s the way I like it"
date: 2018-08-03T19:10:07+01:00
description: "I have been stumped as to how to make my url over dat:// friendly, because dat://59278ae8cc9d27492b4a0a5d4392ff3c913aebd8fa079760ce732f26b70c4eef (open in BeakerBrowser) is just mean."
url: "blog/dats-the-way-i-like-it"
keyword: "dat"
tags: [redesign, dat]
images:
- /blog/dats-the-way-i-like-it/screenshot-2018-08-03-19-29-24.png
---

<iframe src="https://www.youtube-nocookie.com/embed/k_ZPaQUS8W4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

I committed to supporting a decentralised peer-to-peer web by serving my site over **dat://** as well as **https://**. Since starting this I have been stumped as to how to make my url over dat:// friendly, because [dat://59278ae8cc9d27492b4a0a5d4392ff3c913aebd8fa079760ce732f26b70c4eef](dat://59278ae8cc9d27492b4a0a5d4392ff3c913aebd8fa079760ce732f26b70c4eef) *(open in [BeakerBrowser](https://beakerbrowser.com))* is just mean.

I could see other sites had achieved this. Beaker's own site does exactly what I wanted with [dat://beakerbrowser.com/](dat://beakerbrowser.com/) *(open in [BeakerBrowser](https://beakerbrowser.com))*.

I read several docs about it and kept getting confused to the point that I'd give up, hoping to return later with a fresh mind and try again.

Eventually, I have succeeded and it is ridiculously easy so I'll share a simple step-by-step guide so you don't have to do through the same pain.

1. Create a folder called `.well-known` in the root of your https:// served directory.
2. Create a file called `dat` in that folder.
3. Add the following code to that file, replacing {key} with the mega long dat URL key to your site. Fe, dat://59278ae8cc9d27492b4a0a5d4392ff3c913aebd8fa079760ce732f26b70c4eef
  {{< highlight html >}}
  dat://{key}
  TTL=60{{< /highlight >}}
4. Save and upload to wherever you host your https:// site.
5. Now, visit your site in [BeakerBrowser](https://beakerbrowser.com) but use dat:// instead of https:// as the URL and automagically it works 🎉

{{< figure src="screenshot-2018-08-03-19-29-24.png" caption="dat://charlesrt.uk is finally working and it turns out it was easy peasy hotdog squeezy." >}}
