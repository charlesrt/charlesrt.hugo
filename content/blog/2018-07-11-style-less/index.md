---
title: "Style.less"
date: 2018-07-11T09:54:15+01:00
url: "blog/style-less"
keyword: "style"
tags: [redesign, css]
---

{{< figure src="screenshot-2018-07-12-08-48-00.png" caption="No style here" >}}

Right now, things are looking a little basic.

{{< highlight css >}}

Style Attribute {
  🤷‍♂️
}

{{< /highlight >}}

The plan was to get the website up and running and let your default browser styles do the work, then carefully layer on top the bare minimum required to achieve the [design goals](/blog/design-goals) I set.

You'd think I would have done more research into [Hugo](https://gohugo.io) before choosing it as the framework to rebuild my website. It turns out it doesn't natively do any CSS preprocessing. I don't know why I assumed it did---I guess because so many [static site generators](http://staticgen.com/) do.

This isn't the end of the world---I do know how to write in vanilla CSS---I've just gotten used to writing in syntax, like [less](http://lesscss.org), and taking advantage of the power that has to offer, such as variables, mixins, etc.

Not to worry. I'll take a small break to add some sort of [Gulp](https://gulpjs.com) task and do some of the mundane work that needs doing first; like migrating all my old blog posts and making sure redirects are in place so as not to break the web.
