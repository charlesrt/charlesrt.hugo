---
title: "The Dark (Mode) Web Rises"
date: 2018-11-28T20:50:37Z
description: "Part 3 in a series about why we need Dark Mode to be an inclusive design consideration. Using CSS media query to detect if a user has set system-level preferences to use a light or dark colour theme."
url: "blog/the-dark-web-rises"
keyword: "dark"
tags: [dark, inclusion, accessibility]
hero: "/blog/the-dark-web-rises/dark-web-rises__transparent.png"
color:
  base: "#001F3F"
  alt: "#FFDC00"
images:
- /blog/the-dark-web-rises/dark-web-screenshot.png
---

*This is part 3 in a series about why we need Dark Mode to be an inclusive design consideration.*

1. [Molly Watt](/blog/solving-molly-watts-problems)
2. [Apple need a Dark Mode](/blog/apple-need-a-dark-mode-for-people-like-molly-watt/)
3. The Dark (Mode) Web Rises

In 2016, I met Molly Watt.

I was inspired by Molly and her experience of living with [Usher Syndrome](http://www.molly-watt-trust.org); a condition that has left her deaf with 5--degree vision in one eye. [I wrote about my immediate desire to solve some of the problems Molly faces with using digital products](/blog/solving-molly-watts-problems).

Then, in 2017, [I wrote a follow up post on how Apple (and others) need a Dark Mode](/blog/apple-need-a-dark-mode-for-people-like-molly-watt/); a system-level preference setting that would affect the interfaces of native, third-party apps and websites.

This year (2018), we were gifted with an update to MacOS: Mojave. Mojave shipped with a system-level **Dark Mode** preference setting. Finally!

Turning on Dark Mode in Mojave made instant changes to the system interface and the native Mac apps. Soon after, third-party apps started to release updates that took advantage of this preference setting too; making their interfaces work in Dark Mode. But, there was still a gap: websites.

Whilst the Safari app supported Dark Mode, there was no change to the content of the websites viewed through it. Glaring white websites were still glaringly white. It felt like a giant leap in the right direction but a small step short: until now.

## Give yourself to the dark side

Please, allow me to introduce you to the `prefers-color-scheme` CSS media query.

Media queries are used to modify a website depending on specific characteristics (print, size, orientation, etc).

Now, we have one to detect if a user has set system-level preferences to use a light or dark colour theme.

{{< figure src="dark-web-demo.gif" caption="MacOS Mojave has system-level colour preferences" >}}

This feature is not yet ready for use on general release browsers but it's coming soon. [Safari Technology Preview 68](https://webkit.org/blog/8475/release-notes-for-safari-technology-preview-68/) added support last month so hopefully, it won't be long before this finds its way into a mainstream Safari release.

## Add support for Dark Mode on your website

Developers can (and should) start getting their websites ready for Dark Mode. You can install [Safari Technology Preview](https://developer.apple.com/safari/download/) now and start designing the perfect Dark Mode theme using colours that compliment your brand and work consistently across your website.

A simple example, to get you started:

{{< highlight css >}}

@media screen and (prefers-color-scheme: dark) {
  body {
    background-color: black;
    color: white;
  }
}

{{< /highlight >}}

## Dark by default

You might notice that my own website is dark by default. I have made this decision as it's not only my preference but I know it works better for Molly and for users in certain scenarios (browsing at night, migraines, hangovers, etc). Designing for the few, makes things better for the many.

I've used the `prefers-color-scheme` CSS media query to target the `light` preference. If you are using [Safari Technology Preview](https://developer.apple.com/safari/download/) on Mojave and have your preferences set to `light` you'll see the light theme of my website.

You do this using:

{{< highlight css >}}

@media screen and (prefers-color-scheme: light) {
  body {
    background-color: white;
    color: black;
  }
}

{{< /highlight >}}

## The future is dark (a good thing)

I am a designer at the UK government. This series of posts started as a result of wanting to influence the design of [GOV.UK](http://gov.uk) (a typically white website) so that it works better for users like Molly Watt.

So far, I haven't got very far with this. Every potential solution seemed to come with a degree of risk and complication when considering the design principle: [this is for everyone](https://www.gov.uk/guidance/government-design-principles#this-is-for-everyone).

I reckon that using `prefers-color-scheme` CSS media query is a risk-free low-impact solution. It will give [GOV.UK](http://gov.uk) a Dark Mode for the users that prefer one and remain the same for everyone else. I'm going to prototype this and start pushing to make it happen.

I'm hopeful that, the more websites (and big brands) that implement and blog about their Dark Modes, the more chance we'll have of seeing this feature across all browsers (not just Safari).

Other platforms need to start introducing their own system-level Dark Mode preference setting to work this way too.

**Look out for a similar feature coming to iOS soon.**

I can't wait to see how websites take advantage of this new CSS media query. Share how you use it: tweet me [@charles_rt](https://twitter.com/charles_rt/) or toot me [@crt](https://mastodon.social/@crt).
