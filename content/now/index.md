---
title: "Now"
date:
url: "now"
keyword: "now"
tags: [now]
---

Right now, I'm building my website in the open. You can find out more about why I'm doing this in [Hello World](/blog/hello-world) or follow the updates to the style design in [This is my baseline](/blog/this-is-my-baseline).

If that doesn't take your fancy, you can read other [words on my blog](/blog) or follow me [on Twitter](https://twitter.com/charles_rt).

Thanks, for stopping by.
