var gulp = require('gulp');
var stylus = require('gulp-stylus');

gulp.task('styles', done => {
  gulp.src('src/*.styl')
      .pipe(stylus())
      .pipe(gulp.dest('static/'));
  done();
});

gulp.task('default', function() {
  gulp.watch('src/**/*.styl', gulp.series(['styles']));
});
